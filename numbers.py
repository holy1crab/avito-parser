import uuid, re, time

p = re.compile(r'\d{10,11}')


def correct_number(number):
    number = str(number).strip()

    m = p.search(number)
    if not m:
        return None
    else:
        number = m.group()

    l = len(number)
    if l == 11:
        if number[0:2] == '79':
            return int(number)
        elif number[0:2] == '89':
            return int('7' + number[1:])
    elif l == 10 and number[0] == '9':
        return int('7' + number)
    else:
        return None

if __name__ == '__main__':
    begin = time.time()
    for i in range(100000):
        correct_number("+79082728129"), i
    print(time.time() - begin)
