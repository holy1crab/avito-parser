import math
import io
import re
import os
import random
import time
import datetime
from collections import defaultdict

from PIL import Image
from lxml import etree
import requests
import pymongo
import bson
import urllib.request, urllib.error
import socket
import gridfs

import tag_map

import numbers

WHITE = (255, 255, 255)
CLOSE_WHITE = (120, 120, 120)

DIGIT_PERCENT = {
    0: {(0, 0): 67, (0, 1): 67, (0, 2): 67, (1, 0): 50, (1, 1): 100, (1, 2): 67, (2, 0): 62, (2, 1): 75, (2, 2): 67},
    1: {(0, 0): 67, (0, 1): 67, (0, 2): 0, (1, 0): 100, (1, 1): 100, (1, 2): 0, (2, 0): 75, (2, 1): 75, (2, 2): 0},
    2: {(0, 0): 67, (0, 1): 67, (0, 2): 56, (1, 0): 100, (1, 1): 100, (1, 2): 67, (2, 0): 50, (2, 1): 62, (2, 2): 75},
    3: {(0, 0): 67, (0, 1): 67, (0, 2): 44, (1, 0): 100, (1, 1): 67, (1, 2): 67, (2, 0): 75, (2, 1): 75, (2, 2): 67},
    4: {(0, 0): 100, (0, 1): 83, (0, 2): 33, (1, 0): 67, (1, 1): 83, (1, 2): 50, (2, 0): 75, (2, 1): 75, (2, 2): 38},
    5: {(0, 0): 33, (0, 1): 67, (0, 2): 67, (1, 0): 50, (1, 1): 67, (1, 2): 78, (2, 0): 75, (2, 1): 75, (2, 2): 67},
    6: {(0, 0): 67, (0, 1): 67, (0, 2): 78, (1, 0): 33, (1, 1): 67, (1, 2): 89, (2, 0): 62, (2, 1): 75, (2, 2): 67},
    7: {(0, 0): 67, (0, 1): 67, (0, 2): 44, (1, 0): 100, (1, 1): 100, (1, 2): 67, (2, 0): 88, (2, 1): 62, (2, 2): 100},
    8: {(0, 0): 50, (0, 1): 67, (0, 2): 56, (1, 0): 67, (1, 1): 67, (1, 2): 56, (2, 0): 38, (2, 1): 75, (2, 2): 67},
    9: {(0, 0): 67, (0, 1): 67, (0, 2): 67, (1, 0): 83, (1, 1): 67, (1, 2): 44, (2, 0): 88, (2, 1): 75, (2, 2): 67},
}

def get_and_save_image(url):

    headers = {
        'Referer': url
    }

    r = requests.get(url, headers=headers)

    print(r.content)

    filename = str(random.randint(1000, 9999)) + '.png'

    with open(filename, 'wb') as f:
        f.write(r.content)

    return filename


def __get_longest_line_data(l):
    data, _max = [0, None], [0, 0]
    for index, p in enumerate(l):
        if p == 1:
            if data[1] is None:
                data[1] = index
            data[0] += 1
        else:
            if data[0] > _max[0]:
                _max = data
                data = [0, None]
    return _max

def _get_coord(image, offset):

    width, height = image.size

    left, top, right, bottom = 0, 0, 0, 0

    first_left = False

    has_pixels = False

    is_four = False

    for x in range(offset, width):

        if first_left:

            pixels = []

            for y in range(height):
                if image.getpixel((x, y)) < CLOSE_WHITE:
                    pixels.append(1)
                else:
                    pixels.append(0)

            dark_pixels_len = len(list(filter(lambda x: x == 1, pixels)))

            if dark_pixels_len >= 10: # need fix
                longest_line_length, start_index = __get_longest_line_data(pixels)
                if image.getpixel((x+1, start_index + longest_line_length - 1)) == WHITE:
                    is_four = True

            if dark_pixels_len == 1:

                if is_four:
                    right = x+1
                else:
                    for index, pixel in enumerate(pixels):
                        if pixel == 1:
                            next_right = image.getpixel((x+1, index))
                            next_right_top = image.getpixel((x+1, index+1))
                            next_right_bottom = image.getpixel((x+1, index+1))

                            if next_right > CLOSE_WHITE and next_right_top > CLOSE_WHITE and next_right_bottom > CLOSE_WHITE:
                                right = x

            if dark_pixels_len == 0:
                right = x

            if right > 0:
                # scan from top to bottom
                for y in range(height):

                    if top > 0:
                        all_white = True

                        for x in range(right - left):
                            p = image.getpixel((left + x, y))

                            if p < WHITE:
                                all_white = False
                                break

                        if all_white:
                            bottom = y
                            return left, top, right, bottom

                    else:
                        for x in range(right - left):
                            if image.getpixel((left + x, y)) < WHITE and top < y:
                                top = y
                break

        else:
            for y in range(height):

                if image.getpixel((x, y)) < WHITE:
                    has_pixels = True
                    first_left = True
                    left = x
                    break

    if not has_pixels:
        return ()



def image_process(file_name_or_bytes):

    if isinstance(file_name_or_bytes, bytes):
        file_name_or_bytes = io.BytesIO(file_name_or_bytes)

    image = Image.open(file_name_or_bytes)

    offset = 0

    digits = []

    while True:

        coord = _get_coord(image, offset)

        if not coord:
            break

        left, top, right, bottom = coord

        offset = right + 1

        digit = _get_digit(image.crop((left, top, right, bottom)), coord = (left, top, right, bottom))

        if digit is not None:
            digits.append(digit)

    if len(digits) != 11:
        return None

    return ''.join(map(str, digits))


def _find_closest_digits(coord, percent):

    digits = []
    for k, value in DIGIT_PERCENT.items():

        if abs(value[coord] - percent) < 10:
            digits.append(k)

    return digits

def _get_digit(digit_image, coord = None):

    image = digit_image

    width, height = image.size

    C = (127, 127, 127)
    # print('size', width, height)

    step_width = math.floor(width / 3)
    step_height = math.floor(height / 3)

    if width * height < 20:
        return None

    coords = []
    for i in range(3):

        coo = []

        for k in range(3):

            left = k * step_width
            top = i * step_height
            right = (k + 1) * step_width

            if width - right < step_width:
                right += width - right

            bottom = (i + 1) * step_height

            if height - bottom < step_height:
                bottom += height - bottom

            coo.append((left, top, right, bottom))

        coords.append(coo)

    percent = []
    for i in range(len(coords)):
        for j in range(len(coords[i])):

            im = image.crop(coords[i][j])

            width, height = im.size

            white_pixel_count = 0
            total_count = width * height
            for x in range(width):
                for y in range(height):
                    p = im.getpixel((x, y))
                    if p > C:
                        white_pixel_count += 1

            # print(coords[i][j], block_pixel_count, total_count, str(round(float(block_pixel_count)/total_count*100)) + '%')
            percent.append({'coord': (i, j), 'percent': round(float(white_pixel_count)/total_count*100)})

    digit_count = defaultdict(int)

    for item in percent:

        digits = _find_closest_digits(item['coord'], item['percent'])

        for digit in digits:
            digit_count[digit] += 1

        # print(item['coord'], item['percent'])

    _max, digit = 0, None
    for d, count in digit_count.items():
        if count > _max:
            _max = count
            digit = d

    return digit


def __phone_demixer(key, _id):
    pre = re.findall('[0-9a-f]+', key)
    mixed = ''.join((int(_id) % 2 == 0 and pre[::-1]) or pre)
    r = ''
    for index, i in enumerate(mixed):
        if index % 3 == 0:
            r += i
    return r

def _get_detail_urls(url):
    try:
        content = urllib.request.urlopen(urllib.request.Request(url)).read()
    except urllib.error.HTTPError as e:
        if e.code == 404:
            return []
        else:
            raise e
    else:
        parser = etree.HTMLParser()
        tree = etree.parse(io.StringIO(content.decode('utf-8')), parser)
        return list(map(lambda x: x.attrib['href'], tree.xpath('.//*[contains(@class, "b-catalog-table")]//a[contains(@class, "second-link")]')))

def _parse(city, start_page=1):

    page = start_page

    stat = defaultdict(int)

    while True:

        url = 'http://www.avito.ru/{0}?p={1}'.format(city, page)
        detail_urls = _get_detail_urls(url)
        page += 1

        if not len(detail_urls):
            print('All done. Scanned: {0} correct numbers: {1}'.format(stat['scanned'], stat['correct']))
            return

        for href in detail_urls:

            _id = re.findall('\d+$', href, re.IGNORECASE)[0]

            if not Db.canCheck(_id):
                print('_id: {0} continue'.format(_id))
                continue

            content = urllib.request.urlopen(urllib.request.Request('http://avito.ru' + href)).read()
            stat['scanned'] += 1

            item_url = re.findall('item_url = \'(.*?)\'', content.decode('utf-8'))[0] # e.g. 'ocher_avtomobili_s_probegom_hyundai_matrix_2006_238414813'
            item_phone = re.findall('item_phone = \'(.*)?\'', content.decode('utf-8'))[0] # e.g. 2bd4e6rb0cr81a27197ar8ce2ff8d0770rc5d8282c93aerd991eb3be5991ea79cr582fd5407f0daffaec8a7rc17r9a1r5c0a6e0db

            image_url = 'http://www.avito.ru/items/phone/{0}?pkey={1}'.format(item_url, __phone_demixer(item_phone, _id))

            image_content = urllib.request.urlopen(urllib.request.Request('http://avito.ru' + href, headers={'Referer': image_url})).read()
            # r = session.get(image_url, headers={'Referer': image_url})

            number = image_process(image_content)
            if number is not None:
                number = numbers.correct_number(number)
                stat['correct'] += 1

            with open(os.path.join('pic', '{0}.png'.format(_id)), 'wb') as f:
                f.write(image_content)

            db_id = Db.save(_id, number, href, item_url, city, image=image_content)
            print('{0} {1} {2}'.format(db_id, str(number), href))

            time.sleep(2)
            # break
        # break

def _parse_mini(city, category = None, start_page=1):
    page = start_page
    host = 'm.avito.ru'

    if category:
        path = city + '/' + category
    else:
        path = city

    stat = defaultdict(int)

    while True:

        url = 'http://{0}/{1}?page={2}'.format(host, path, page)
        page += 1

        print('url: {0}'.format(url))

        try:
            content = urllib.request.urlopen(urllib.request.Request(url)).read()
        except urllib.error.HTTPError as e:
            if e.code == 404:
                return print('All done. Scanned: {0} correct numbers: {1}'.format(stat['scanned'], stat['correct']))
            else:
                raise e
        parser = etree.HTMLParser()
        tree = etree.parse(io.StringIO(content.decode('utf-8')), parser)
        detail_urls = list(map(lambda x: x.attrib['href'], tree.xpath('.//article[contains(@class, "b-item")]//a[contains(@class, "item-link")]')))
        for href in detail_urls:

            _id = re.findall('\d+$', href, re.IGNORECASE)[0]

            if not Db.canCheck(_id):
                print('_id: {0} continue'.format(_id))
                continue

            stat['scanned'] += 1

            content = urllib.request.urlopen(urllib.request.Request('http://' + host + href)).read()

            url = 'http://' + host
            detail_url = url + href
            number_url = url + re.findall(href + '/phone/[a-zA-Z0-9]+', content.decode('utf-8'), re.IGNORECASE)[0] + '?async'
            headers = {'Host': host, 'Referer': detail_url}
            content = urllib.request.urlopen(urllib.request.Request(number_url, headers=headers)).read()

            raw_number = content.decode('utf-8').replace('-', '').replace(' ', '')
            number = numbers.correct_number(raw_number)
            if number:
                stat['correct'] += 1

            db_id = Db.save(_id, number, href, '', city, category=category, raw_number=raw_number)
            # db_id = None
            print('{0} {1} ({2}) {3}'.format(db_id, str(number), raw_number, href))

            time.sleep(random.randint(2, 3))

        # break


class Db(object):

    db = pymongo.MongoClient(host='localhost').board
    fs = gridfs.GridFS(db)

    @staticmethod
    def canCheck(_id):
        item = Db.db.avito.find_one({'_id': int(_id)})
        return item is None or item['number'] is None and datetime.datetime.utcnow() - item['last_checked'] > datetime.timedelta(minutes=12 * 60)

    @staticmethod
    def save(avito_id, number, details_url, item_url, city, category=None, raw_number=None, image=None):
        if image is not None:
            fs_id = Db.fs.put(image)
        else:
            fs_id = None

        tags = [city, 'avito.ru', category]

        if category is not None:
            tags.append(tag_map.do(category))

        item = {
            '_id': int(avito_id),
            'fs_id': fs_id,
            'number': number,
            'raw_number': raw_number,
            'details_url': details_url,
            'item_url': item_url,
            'city': city,
            'last_checked': datetime.datetime.utcnow(),
            'tags': list(set(tags))
        }
        return Db.db.avito.insert(item)



if __name__ == '__main__':

    _parse_mini('perm', category='transport', start_page=1)

