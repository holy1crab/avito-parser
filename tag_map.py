
_map = {
    'transport': 'auto',
}

def do(value):
    if value in _map:
        return _map[value]
    return value

